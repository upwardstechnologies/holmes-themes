<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>

</head>

<body>
	
	<div class="portcontain" style="">
	
        <div id="slides">
            <div class="slides_container">
                <div>
	                <a href="#" class="next" style="z-index:999;"><img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" /></a>
	                <a href="#" class="next"><img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/08_Sports/01_UWFootball/UWfootball_1.jpg" /></a>
	                <div class="innerback">&nbsp;</div>
                </div>
                <div>
	                <a href="#" class="next" style="z-index:999;"><img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" /></a>
	                <a href="#" class="next"><img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/08_Sports/01_UWFootball/UWfootball_2.jpg" /></a>
             	    <div class="innerback">&nbsp;</div>
                </div>
                <div>
	                <a href="#" class="next" style="z-index:999;"><img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" /></a>
	                <a href="#" class="next"><img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/08_Sports/01_UWFootball/UWfootball_3.jpg" /></a>
                	<div class="innerback">&nbsp;</div>
                </div>
                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0102_nintendo.php" target="_self" style="z-index:999;"><img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" /></a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0102_nintendo.php" target="_self"><img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/08_Sports/01_UWFootball/UWfootball_4.jpg" /></a>
	                <div class="innerback">&nbsp;</div>
                </div>
            </div>
        </div>

			
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					UNIVERSITY OF WASHINGTON FOOTBALL STADIUM & OPERATIONS
				</h4>

				<div class="portcontain_date" style="">Completion Date: August 2013 (in process)</div>

				<p>
					<ul class="portcontain_ul">
						<li>720,000 SQFT Stadium / 80,000 SQFT Team Operations</li>
						<li>Design-Build / Network Infrastructure, Public Address, Broadcast, Security, Audio Visual, IPTV and Distributed Antenna Systems.</li>
						<li>Wright Runstad / 360 Architects / Turner Construction</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>