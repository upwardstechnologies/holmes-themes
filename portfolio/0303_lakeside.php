<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>
    
</head>

<body>
	
	<div class="portcontain" style="">
	
<div id="slides">
            <div class="slides_container">

	            <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0501_childrenshope.php" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0501_childrenshope.php">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/02_Education/03_lakeside/Holmes_Lakeside.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0102_nintendo.php" target="_self" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0102_nintendo.php" target="_self">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/02_Education/02_overlake/OverlakeSchool_2.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
	            </div>

        </div>
		
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					LAKESIDE ATHLETIC CENTER
				</h4>

				<div class="portcontain_date" style=""></div>

				<p>
					<ul class="portcontain_ul">
						<li>63,500 sf, 2-story athletic facility.</li>
						<li>New 800 amp distribution system with specialty network, new lighting system, fire alarm detection, notification and voice evacuation, tele-data and audio visual system.</li>
						<li>Lease Crutcher Lewis</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>