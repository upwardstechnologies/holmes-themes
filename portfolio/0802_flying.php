<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>
    
</head>

<body>
	
	<div class="portcontain" style="">
	
<div id="slides">
        <div class="slides_container">

	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/02_FlyingHeritage/Flying_1.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>
	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/02_FlyingHeritage/Flying_2.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>
  
                 <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/02_FlyingHeritage/Flying_3.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>
                
   	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/02_FlyingHeritage/Flying_4.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>
                
                <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/02_FlyingHeritage/Flying_5.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0803_emp.php" target="_self" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0803_emp.php" target="_self">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/02_FlyingHeritage/Flying_6.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
	            </div>

        </div>
		
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					THE FLYING HERITAGE COLLECTION
				</h4>

				<div class="portcontain_date" style="">Completed: 2008</div>

				<p>
					<ul class="portcontain_ul">
						<li>Renovation of Hangar 207 at Payne Field, Everett.</li>
						<li>Design-Build / Electrical Systems, Network Infrastructure and Security Infrastructure</li>
						<li>Bayley Construction / Owen Richards Architect</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>