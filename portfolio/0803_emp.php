<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>
    
</head>

<body>
	
	<div class="portcontain" style="">
	
<div id="slides">
        <div class="slides_container">

	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/03_EMP/EMP_1.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>


	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/03_EMP/EMP_2.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/03_EMP/EMP_3.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>


	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/03_EMP/EMP_4.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/03_EMP/EMP_5.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0301_stlouise.php" target="_self" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0301_stlouise.php" target="_self">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/07_Cultural/03_EMP/EMP_6.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
	            </div>

        </div>
		
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					EXPERIENCE THE MUSIC PROJECT (EMP)
				</h4>

				<div class="portcontain_date" style="">Completed: 1997</div>

				<p>
					<ul class="portcontain_ul">
						<li>140,000 SQFT 5-Story museum with unique architectural challenges</li>
						<li>Electrical Systems</li>
						<li>Hoffman Construction / Gehry and Associates Architect</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>