<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>
    
</head>

<body>
	
	<div class="portcontain" style="">
	
<div id="slides">
            <div class="slides_container">

	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/ChildrensHope/Hope_1.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>
                
                <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/ChildrensHope/Hope_2.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/ChildrensHope/Hope_3.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0502_childrensambulatory.php" target="_self" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0502_childrensambulatory.php" target="_self">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/ChildrensHope/Hope_4.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
	            </div>

        </div>
		
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					SEATTLE CHILDRENS HOSPITAL BUILDING HOPE
				</h4>

				<div class="portcontain_date" style="">Completion Date: March 2013 (in progress)</div>

				<p>
					<ul class="portcontain_ul">
						<li>330,000 SQFT Critical Care Facility and Emergency Department</li>
						<li>Network Infrastructure, Public Address, Security, Paging, Nurse Call Intercom and Distributed Antenna Systems</li>
						<li>Sellen Construction / ZGF</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>