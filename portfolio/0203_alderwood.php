<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>
    
</head>

<body>
	
	<div class="portcontain" style="">
	
<div id="slides">
            <div class="slides_container">

	            <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/01_Retail/03_AlderwoodMall/Alderwood_1.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
                </div>

                <div>
	                <a href="#" class="next" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="#" class="next">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/01_Retail/03_AlderwoodMall/Alderwood_2.jpg" />
	                </a>
             	    <div class="innerback">&nbsp;</div>
                </div>

                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0204_nordstroms.php" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0204_nordstroms.php">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/01_Retail/03_AlderwoodMall/Alderwood_3.jpg" />
	                </a>
                	<div class="innerback">&nbsp;</div>
                </div>

                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0204_nordstroms.php" target="_self" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0204_nordstroms.php" target="_self">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/01_Retail/03_AlderwoodMall/Alderwood_4.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
	            </div>

        </div>

		<img src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/00_Featured/02_Nintendo/Nintendo_1.jpg" />
		
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					ALDERWOOD MALL
				</h4>

				<div class="portcontain_date" style="">Completion Date:  2006</div>

				<p>
					<ul class="portcontain_ul">
						<li>195,000 SQFT; 7-Building retail village expansion</li>
						<li>Electrical and Network Infrastructure</li>
						<li>Bayley Construction / Callison</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>