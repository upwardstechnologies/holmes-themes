<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>
    
</head>

<body>
	
	<div class="portcontain" style="">
	
<div id="slides">
            <div class="slides_container">

                <div>
	                
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0801_lemay.php" target="_self" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0801_lemay.php" target="_self">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/03_Commercial/06_tumwater/Tumwater_1.jpg" />
	                </a>
	                
	                <div class="innerback">&nbsp;</div>
	                
	            </div>

        </div>
		
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					TUMWATER OFFICE SPACE
				</h4>

				<div class="portcontain_date" style="">Completion Date:  2005</div>

				<p>
					<ul class="portcontain_ul">
						<li>200,000 SQFT Office Building</li>
						<li>Design-Build / Electrical and Network Infrastructure</li>
						<li>Turner Construction</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>