<html>

<head>

	<title>Holmes Electric Portfolio Sub Frame</title>

	<link rel="stylesheet" type="text/css" media="all" href="../../../../../../wp-content/themes/twentyeleven/portfolio/portfolio.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="slides.js"></script>
	
	<script>
        $(function(){
            $("#slides").slides({
	            next: 'next'
	            });
        });
    </script>
    
</head>

<body>
	
	<div class="portcontain" style="">
	
<div id="slides">
        <div class="slides_container">

                <div>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0701_broadmoor.php" target="_self" style="z-index:999;">
	                	<img style="z-index:999;" class="nextarrow"  src="../../../../../wp-content/themes/twentyeleven/images/arrow.png" onMouseOver="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow_hover.png' " onMouseOut="src='../../../../../../wp-content/themes/twentyeleven/portfolio/images/arrow.png'" />
	                </a>
	                <a href="../../../../../../wp-content/themes/twentyeleven/portfolio/0701_broadmoor.php" target="_self">
	                	<img width="719" height="597" src="../../../../../../wp-content/themes/twentyeleven/portfolio/images/05_MissionCritical/04_Nintendo/NintendoData_1.jpg" />
	                </a>
	                <div class="innerback">&nbsp;</div>
	            </div>

        </div>
		
		<div class="portcontain_inner" style="">

			<div class="portcontain_content" style="">

				<h4 class="subhead" style="">
					NINTENDO OF AMERICA
				</h4>

				<div class="portcontain_date" style="">Completion Date:  2010</div>

				<p>
					<ul class="portcontain_ul">
						<li>10,000 SQFT Private Data Center</li>
						<li>Electrical, Network Infrastructure, and Security</li>
						<li>Turner Construction / ZGF</li>
					</ul>
				</p>

			</div>
			
		</div>
		
	</div>

</body>

</html>