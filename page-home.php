<?php
/*
 Template Name: Homepage
*/
?>
<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
                                <div class="bgblock">
                                    <div class="cs col-cs-3 d-2of5 t-2of4">
                                        <div class="banner">
                                            WE ARE<br />
                                            WIRED<br />
                                            RIGHT.
                                        </div>
                                        <div class="span-1">
                                            What It Means >
                                        </div>
                                    </div>
                                    <div class="col-cs-7 cs d-3of5 t-3of5">
                                        <iframe src="http://player.vimeo.com/video/53362147" width="630" height="360" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                    </div>
                                </div>
                                <div class="bgblock">
                                        <div class="mobile">
                                            <h2 class="nodehead"><a href="../services">OUR SERVICES</a></h2>
                                            <h2 class="nodehead"><a href="../portfolio">FEATURED PROJECTS</a></h2>
                                            <h2 class="nodehead">  <a href="../about">WHY HOLMES </a></h2>
                                        </div>
                                        <div class="desktop">
                                            <div class="img-3 cs d-1of3 t-1of3">
                                                <h2 class="nodehead">OUR SERVICES</h2>
                                                <a href="../services">
                                                    <img src="<?php echo get_template_directory_uri();?>/library/images/homeslide1.png" onmouseover="this.src='<?php echo get_template_directory_uri();?>/library/images/homeslide1_hover.png'" onmouseout="this.src='<?php echo get_template_directory_uri();?>/library/images/homeslide1.png'" />
                                                </a>
                                            </div>
                                            <div class="img-3 cs d-1of3 t-1of3">
                                                <h2 class="nodehead">FEATURED PROJECTS</h2>
                                                <a href="../portfolio">
                                                    <img src="<?php echo get_template_directory_uri();?>/library/images/homeslide2.png" onmouseover="this.src='<?php echo get_template_directory_uri();?>/library/images/homeslide2_hover.png'" onmouseout="this.src='<?php echo get_template_directory_uri();?>/library/images/homeslide2.png'" />
                                                </a>
                                            </div>
                                            <div class="img-3 cs d-1of3 t-1of3">
                                                <h2 class="nodehead">WHY HOLMES</h2>
                                                <a href="../about">
                                                    <img src="<?php echo get_template_directory_uri();?>/library/images/homeslide3.png" onmouseover="this.src='<?php echo get_template_directory_uri();?>/library/images/homeslide3_hover.png'" onmouseout="this.src='<?php echo get_template_directory_uri();?>/library/images/homeslide3.png'" />
                                                </a>
                                            </div>
                                        </div>

                                </div>
                                <div class="tag-footer desktop">
                                    <h2>
                                        CONNECTING THE PUGET SOUND FOR OVER 65 YEARS.
                                    </h2>
                                </div>
                                <div class="bgblock cs-block">
                                    <div class="latestnews d-1of2 t-1of2">
                                        <h2 class="subhead">LATEST NEWS &amp; UPDATES</h2>
                                         <?php
                                         $temp_query = $wp_query;
                                         query_posts('showposts=2');
                                         while (have_posts()) : the_post();
                                         ?>
                                             <div class="latestitem" id="post-<?php the_ID(); ?>">
                                                 <div class="thumb-item"><?php the_post_thumbnail('thumbnail'); ?></div>
                                                 <div class="title-news" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></div>
                                                 <div class="date-news"><?php the_time(); ?></div>
                                                 <div class="content-news"><?php the_excerpt(); ?></div>
                                             </div>
                                         <?php endwhile; ?>
                                    </div>
                                    <div class="socialhome d-1of2 t-1of2">
                                        <h2 class="subhead">LIVE CONSTRUCTION CAMS</h2>
                                        <div style="margin-bottom:22px;">
                                            <a href="http://oxblue.com/open/huskystadium" target="_blank"><img style="vertical-align:middle;" src="<?php echo get_template_directory_uri();?>/library/images/playstream_ico.png" />&nbsp;&nbsp;UW Jobsite</a> Live Cam<br />
                                        </div>
                                        <h2 class="subhead" style="margin-top:8px;">STAY IN TOUCH WITH HOLMES</h2>

                                        <div style="margin-bottom:10px;margin-top:8px;">
                                            <a target="_blank" href="http://www.facebook.com/WiredRight"><img style="vertical-align:middle;" src="<?php echo get_template_directory_uri();?>/library/images/like_ico.png" />&nbsp;&nbsp;Holmes Electric</a> on Facebook<br />
                                        </div>

                                        <div style="margin-bottom:10px;">
                                            <a target="_blank" href="http://www.youtube.com/user/HolmesElectric1"><img style="vertical-align:middle;" src="<?php echo get_template_directory_uri();?>/library/images/youtube_ico.png" />&nbsp;&nbsp;Holmes TV</a> on YouTube<br />
                                        </div>

                                        <div style="margin-bottom:10px;">
                                            <a target="_blank" href="http://www.linkedin.com/company/3204297?trk=tyah"><img style="vertical-align:middle;" src="<?php echo get_template_directory_uri();?>/library/images/linkedcall.png" />&nbsp;&nbsp;Holmes Electric</a> on LinkedIn<br />
                                        </div>
                                    </div>
                                </div>
						</main>

				</div>

			</div>

<?php get_footer(); ?>
