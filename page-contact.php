<?php
/*
 Template Name: Contact
*/
?>
<?php get_header(); ?>

			<div id="content" class="contact">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
                            <div id="top-image">
                                <img  src="<?php echo get_template_directory_uri();?>/library/images/contactus.png" />
                            </div>
                            <div class="bgblock cs-content">
                                <div id="col-nav " class="cs d-2of7">
                                    <h2 class="subhead">HOLMES LOCATIONS</h2>
                                    <br />
                                    <div class="location">
                                        <strong>Corporate Office</strong><br />
                                        600 Washington Ave. South<br />
                                        Kent, WA 98032<br />
                                        <a href="https://maps.google.com/maps?q=600+Washington+Ave.+South+%3FKent,+WA+98032%3F&ie=UTF-8&hq=&hnear=0x5490595926b19ea1:0x47d09a0ed42a73f3,600+Washington+Ave+S,+Kent,+WA+98032&gl=us&ei=2BW8UOmvMNDmiwK1rYD4Cw&ved=0CC8Q8gEwAA" target="_blank">Get Directions</a><br />
                                        <br />
                                        phone: 253.479.4000<br />
                                        fax: 253.234.2900<br />
                                        <br />
                                        <strong>Mailing Address</strong><br />
                                        PO Box 338<br />
                                        Kent, WA 98035<br />
                                        <br />
                                        <strong>Pre-Fab Service Center</strong><br />
                                        18215 Olympic Ave. South<br />
                                        Tukwila, WA 98188-4722<br />
                                        <a href="https://maps.google.com/maps?q=18215+Olympic+Ave.+South%3F+Tukwila,+WA+98188-4722%3F&ie=UTF-8&hq=&hnear=0x54905cebf360589b:0x346246a58d116177,18215+Olympic+Ave+S,+Tukwila,+WA+98188&gl=us&ei=kRW8UNuSJerjigLxlYG4BQ&ved=0CC8Q8gEwAA" target="_blank">Get Directions</a><br />
                                        <br />
                                        <strong>Communications - Oregon</strong><br />
                                        3800 NE Sandy Blvd.<br />
                                        Suite 111<br />
                                        Portland, OR 97232<br />
                                        <br />
                                        phone: 503.284.5530<br />
                                        fax: 503.284.4830<br />
                                    </div>
                                </div>
                                <div class="content socialhome d-5of7">
                                    <?php if (have_posts()) : while (have_posts()) : the_post();?>
                                        <?php the_content(); ?>
                                    <?php endwhile; endif; ?>

                                </div>
                            </div>

						</main>



				</div>

			</div>

<?php get_footer(); ?>
