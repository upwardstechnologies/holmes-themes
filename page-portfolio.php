<?php
/*
 Template Name: Portfolio
*/
?>
<?php get_header(); ?>

			<div id="content" class="portfolio">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
                            <div class="bgblock">
                                <div class="cs col-cs-3 d-2of5 t-2of4">
                                   <div id="title-portfolio">
                                       PORTFOLIO
                                   </div>
                                    <script>
                                        jQuery(document).ready(function () {
                                            jQuery('#portfolio_nav > li > a').click(function(){
                                                if (jQuery(this).attr('class') != 'active'){
                                                    jQuery('#portfolio_nav li ul').slideUp();
                                                    jQuery(this).next().slideToggle();
                                                    jQuery('#portfolio_nav li a').removeClass('active');
                                                    jQuery(this).addClass('active');
                                                }
                                            });
                                        });
                                    </script>
                                    <ul id="portfolio_nav">
                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0101_uwfootball.php">FEATURED PROJECTS</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0101_uwfootball.php">UW Football Stadium and Operations</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0102_nintendo.php">Nintendo of America Headquarters</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0903_seahawks.php">Seattle Seahawks Headquarters and Training Facility</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0102_nintendo.php">COMMERCIAL</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0102_nintendo.php">Nintendo of America Headquarters</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0402_southhill.php">South Hill Business Park</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0403_firstsavings.php">First Savings Bank Loan Center</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0404_bravern.php">The Bravern</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0405_chinook.php">Chinook Office Building</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0406_tumwater.php">Tumwater Office Building</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0801_lemay.php">CULTURAL</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0801_lemay.php">LeMay Car Museum</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0802_flying.php">The Flying Heritage Collection</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0803_emp.php">Experience The Music Project (EMP)</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0301_stlouise.php">EDUCATION</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0301_stlouise.php">Saint Louise Parish</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0302_overlake.php">The Overlake School Humanities Building</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0303_lakeside.php">Lakeside Athletic Center</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0501_childrenshope.php">HEALTHCARE</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0501_childrenshope.php">Seattle Children's Hospital Building Hope</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0502_childrensambulatory.php">Seattle Chindren's Hospital Ambulatory Care Building</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0503_evergreen.php">Evergreen Hospital Neurosciences and Cardiac Health Institute</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0504_overlake.php">Overlake Hospital Medical Office Tower</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0601_msft.php">MISSION CRITICAL</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0601_msft.php">MSFT Columbia</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0602_quincy.php">Intergate Quincy</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0603_east.php">Intergate East</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0604_nintendodata.php">Nintendo of America</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0701_broadmoor.php">PRIVATE CLUBS</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0701_broadmoor.php">Broadmoor Country Club</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0702_seattletennis.php">The Seattle Tennis Club</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0703_seattleyacht.php">The Seattle Yacht Club</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0704_rainier.php">The Rainier Club</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0201_westfield.php">RETAIL</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0201_westfield.php">Westfield Mall - Southcenter</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0202_northgate.php">Northgate Mall</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0203_alderwood.php">Alderwood Mall</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0204_nordstroms.php">Nordstrom Flagship Store</a></li>
                                            </ul>
                                        </li>

                                        <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0101_uwfootball.php">SPORTS</a>
                                            <ul>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0101_uwfootball.php">University of Washington Football Stadium and Operations</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0902_uwbaseball.php">University of Washington Baseball Facility</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0903_seahawks.php">Seattle Seahawks Headquarters and Training Facility</a></li>
                                                <li><a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0904_centurylink.php">Centurylink Field Club Level Remodel</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-cs-7 cs d-3of5 t-3of5">
                                    <iframe style="overflow:hidden;border:none;" scrolling="no" name="portfolioframe" id="portfolioframe" src="<?php echo get_template_directory_uri();?>/portfolio/0101_uwfootball.php"></iframe>
                                </div>
                            </div>
                            <div class="bgblock">
                                <div class="img-3 cs d-1of3 t-1of3">
                                    <h2 class="nodehead">FEATURED PROJECTS</h2>
                                    <a target="portfolioframe" href="<?php echo get_template_directory_uri();?>//portfolio/0101_uwfootball.php">
                                        <img src="<?php echo get_template_directory_uri();?>/library/images/featured1.png" onmouseover="this.src='<?php echo get_template_directory_uri();?>/library/images/featured1_hover.png'" onmouseout="this.src='<?php echo get_template_directory_uri();?>/library/images/featured1.png'" />
                                    </a>
                                </div>
                                <div class="img-3 cs d-1of3 t-1of3">
                                    <h2 class="nodehead">&nbsp;</h2>
                                    <a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0102_nintendo.php">
                                        <img src="<?php echo get_template_directory_uri();?>/library/images/featured2.png" onmouseover="this.src='<?php echo get_template_directory_uri();?>/library/images/featured2_hover.png'" onmouseout="this.src='<?php echo get_template_directory_uri();?>/library/images/featured2.png'" />
                                    </a>
                                </div>
                                <div class="img-3 cs d-1of3 t-1of3">
                                    <h2 class="nodehead">&nbsp;</h2>
                                    <a target="portfolioframe" href="<?php echo get_template_directory_uri();?>/portfolio/0903_seahawks.php">
                                    <img src="<?php echo get_template_directory_uri();?>/library/images/featured3.png" onmouseover="this.src='<?php echo get_template_directory_uri();?>/library/images/featured3_hover.png'" onmouseout="this.src='<?php echo get_template_directory_uri();?>/library/images/featured3.png'" />
                                    </a>
                                </div>
                            </div>

						</main>



				</div>

			</div>

<?php get_footer(); ?>
