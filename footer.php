			<footer id="footer" class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php /*wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); */?>
					</nav>
<span class="mobile">
                        <a href="https://www.facebook.com/WiredRight" target="_blank">
                            <img style="margin-left:12px;" src="<?php echo get_template_directory_uri();?>/library/images/h_fb.png" alt="Facebook" />
                        </a>
                        <a href="http://www.linkedin.com/company/3204297?trk=tyah" target="_blank">
                            <img style="margin-left:4px;" src="<?php echo get_template_directory_uri();?>/library/images/h_li.png" alt="LinkedIn" />
                        </a>
                        <a href="http://www.youtube.com/user/HolmesElectric1" target="_blank">
                            <img style="margin-left:4px;" src="<?php echo get_template_directory_uri();?>/library/images/h_yt.png" alt="YouTube" />
                        </a>
                        </span>
					<p id="site-generator" class="source-org copyright">
                        &copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> | A 4th Avenue Media Production  &nbsp;&nbsp; 	<a href="/">HOME</a> |  <a href="/services">SERVICES</a> | <a href="../../portfolio">PORTFOLIO</a> | <a href="../../about">ABOUT</a> | <a href="/media">MEDIA</a> | <a href="/contact">CONTACT</a> | <a href="/about/careers">CAREERS</a>
                       <span class="desktop">
                        <a href="https://www.facebook.com/WiredRight" target="_blank">
                            <img style="margin-left:12px;" src="<?php echo get_template_directory_uri();?>/library/images/h_fb.png" alt="Facebook" />
                        </a>
                        <a href="http://www.linkedin.com/company/3204297?trk=tyah" target="_blank">
                            <img style="margin-left:4px;" src="<?php echo get_template_directory_uri();?>/library/images/h_li.png" alt="LinkedIn" />
                        </a>
                        <a href="http://www.youtube.com/user/HolmesElectric1" target="_blank">
                            <img style="margin-left:4px;" src="<?php echo get_template_directory_uri();?>/library/images/h_yt.png" alt="YouTube" />
                        </a>
                        </span>
                    </p>

				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
