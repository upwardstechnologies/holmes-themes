<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">
                    <?php //get_sidebar(); ?>
						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
                            <div class="bgblock">
                                <div class="cs col-cs-3 d-2of5 t-2of4">
                                    <div class="banner">
                                        HOLMES<br />
                                        MEDIA<br />
                                    </div>
                                    <div class="span-1">
                                        Our Latest Story >
                                    </div>
                                </div>
                                <div class="col-cs-7 cs d-3of5 t-3of5">
                                    <iframe src="http://player.vimeo.com/video/13528256?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="630" height="360" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                </div>
                            </div>
                            <div class="bgblock cs-content">
                                <div id="col-nav " class="cs d-2of7">
                                    <div class="sidehead" style="margin-bottom:6px;">RECENT VIDEOS</div>
                                    <a onclick="document.getElementById('lightbox').style.display='inline';" href="#"><img src="/wp-content/uploads/2012/12/thumb.jpg" /></a>
                                    <br />
                                    <a onclick="document.getElementById('lightbox').style.display='inline';" href="#"><img style="vertical-align:middle;" src="<?php echo get_template_directory_uri();?>/library/images/playstream_ico.png" width="32" height="24" src="" alt="Play" /> &nbsp;Seahawks Headquarters</a><br />
                                    <br />
                                    <a onclick="document.getElementById('lightbox2').style.display='inline';" href="#"><img src="/wp-content/uploads/2012/12/thumb2.png" /></a>
                                    <br />
                                    <a onclick="document.getElementById('lightbox2').style.display='inline';" href="#"><img style="vertical-align:middle;" src="<?php echo get_template_directory_uri();?>/library/images/playstream_ico.png" width="32" height="24" src="" alt="Play" /> &nbsp;Prefabrication<br />
                                    <br />
                                    <a onclick="document.getElementById('lightbox3').style.display='inline';" href="#"><img src="/wp-content/uploads/2012/12/thumb3.png" /></a>
                                    <br />
                                    <a onclick="document.getElementById('lightbox3').style.display='inline';" href="#"><img style="vertical-align:middle;" src="<?php echo get_template_directory_uri();?>/library/images/playstream_ico.png" width="32" height="24" src="" alt="Play" /> &nbsp;JPAC/SIS</a><br />
                                    <br />
                                    <div class="sidehead">LATEST NEWS</div>
                                        <br />
                                        <?php $temp_query = $wp_query; ?>
                                        <?php query_posts('showposts=2'); ?>
                                        <?php while (have_posts()) : the_post(); ?>
                                            <div class="sidebarnewsitem" id="post-<?php the_ID(); ?>">
                                                <div class="news-item-title">
                                                    <a  href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a>
                                                </div>
                                                <span>-</span> <span><?php the_date('Y/m/d'); ?></span>
                                                <?php the_excerpt(); ?>
                                            </div>

                                        <?php endwhile; ?>
                                        <?php wp_reset_query(); ?>
                                        <br />
                                        <div class="sidehead" style="margin-bottom:6px;">CATEGORIES</div>
                                        <?php wp_list_categories('title_li='); ?>
                                        <br />
                                        <div class="sidehead" style="margin-bottom:6px;">ARCHIVE</div>
                                        <div class="archive-item">
                                            <?php get_archives(); ?>
                                        </div>

                                        <?php //get_sidebar(); ?>
                                </div>
                                <div class="content socialhome latestnews d-5of7">
                                    <h1 class="title">LATEST NEWS</h1>

                                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                        <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

                                            <h1 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
                                            <section class="entry-content cf">
                                                <?php the_content(); ?>
                                            </section>
                                        </article>

                                    <?php endwhile; ?>

                                        <?php bones_page_navi(); ?>

                                    <?php else : ?>

                                        <article id="post-not-found" class="hentry cf">
                                            <header class="article-header">
                                                <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
                                            </header>
                                            <section class="entry-content">
                                                <p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
                                            </section>
                                            <footer class="article-footer">
                                                <p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
                                            </footer>
                                        </article>

                                    <?php endif; ?>


                                </div>
                            </div>


						</main>



				</div>

			</div>


<?php get_footer(); ?>
