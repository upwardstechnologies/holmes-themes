<?php
/*
 Template Name: About
*/
?>
<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
                            <div class="bgblock">
                                <div class="cs col-cs-3 d-2of5 t-2of4">
                                    <div class="banner">
                                        THE TOP<br />
                                        OF OUR<br />
                                        FIELD.
                                    </div>
                                    <div class="span-1">
                                        New Technologies >
                                    </div>
                                </div>
                                <div class="col-cs-7 cs d-3of5 t-3of5">
                                    <iframe src="http://player.vimeo.com/video/13528256?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="630" height="360" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                </div>
                            </div>
                            <div class="bgblock cs-content">
                                <div id="col-nav " class="cs d-2of7">
                                    <h2 class="subhead">ABOUT HOLMES</h2>
                                    <ul class="subnav">
                                        <?php wp_list_pages( array('title_li'=>'','include'=>get_post_top_ancestor_id()) ); ?>
                                        <?php wp_list_pages( array('title_li'=>'','depth'=>1,'child_of'=>get_post_top_ancestor_id(),'sort_column'=>'menu_order') ); ?>
                                    </ul>
                                </div>
                                <div class="content socialhome d-5of7">
                                    <?php if (have_posts()) : while (have_posts()) : the_post();?>
                                        <?php the_content(); ?>
                                    <?php endwhile; endif; ?>

                                </div>
                            </div>

						</main>



				</div>

			</div>

<?php get_footer(); ?>
